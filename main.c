#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/select.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include "timer_linux.h"
#include "mtimer.h"
#include "ruart.h"
#include "socket_udp.h"

#define MAVLINK_UDP_MAX 8

// https://mavlink.io/en/guide/serialization.html

struct {
	struct sockaddr addr;
	mtimer_t timer;
} mavlink_udp_list[MAVLINK_UDP_MAX];

static uint8_t mavlink_recv_buffer[512];
static int mavlink_recv_len, mavlink_recv_loc;

static char *addr_cstr(struct sockaddr *addr);
static int addr_broadcast(struct sockaddr *addr);
static int addr_isequal(struct sockaddr *addr1, struct sockaddr *addr2);
static uint16_t crc16_byte(uint16_t crc, uint8_t data);
static int mavlink2(uint8_t *buf, uint8_t seq, uint8_t sys_id, uint8_t comp_id, uint32_t msg_id, uint8_t *payload, int len, uint8_t crc_extra);
static void event(char *cmd, int type, int on);

// mavdriver /dev/ttyS1 57600 8 N 1 192.168.2.1 14551 /opt/mavdriver_event.sh
// argv      1          2     3 4 5 6           7
int main(int argc, char *argv[])
{
	struct addrinfo hints, *result;
	struct sockaddr addr;
	uint8_t buffer[2048];
	int fd_uart, fd_udp;
	struct timeval tv;
	mtimer_t uart_timer;
	mtimer_t udp_timer;
	mtimer_t broadcast_timer;
	int uart_ok, udp_ok;
	uint32_t ping_seq = 0;
	uint64_t timestamp;
	uint8_t mav_seq = 0;
	char *cmd;
	fd_set fds;
	int parity;
	int i, l, m;
	socklen_t t;

	if (argc < 8) {
		printf("args\n");
		return 1;
	}

	timer_linux_init();
	mtimer_init(timer_linux_get);

	if (argc > 8)
		cmd = argv[8];
	else
		cmd = NULL;

	event(cmd, 0, 0);
	event(cmd, 1, 0);
	mtimer_timeout_clear(&uart_timer);
	mtimer_timeout_clear(&udp_timer);
	mtimer_timeout_clear(&broadcast_timer);
	uart_ok = udp_ok = 0;

	mavlink_recv_len = 4;
	mavlink_recv_loc = 0;

	fd_uart = ruart_open(argv[1]);
	if (fd_uart < 1) {
		printf("uart open\n");
		return 1;
	}

	parity = RUART_PARITY_NONE;
	if (!strcmp(argv[4], "E"))
		parity = RUART_PARITY_EVEN;
	else if (!strcmp(argv[4], "O"))
		parity = RUART_PARITY_ODD;
	else if (!strcmp(argv[4], "M"))
		parity = RUART_PARITY_MARK;
	else if (!strcmp(argv[4], "S"))
		parity = RUART_PARITY_SPACE;

	ruart_setup(fd_uart, atoi(argv[2]), atoi(argv[3]), parity, atoi(argv[5]), RUART_FLOW_NONE);

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;
	if (getaddrinfo(argv[6], argv[7], &hints, &result)) {
		printf("resolv\n");
		return 1;
	}

	printf("TARGET: %s\n", addr_cstr(result->ai_addr));

	fd_udp = socket_udp_bind(NULL, "14540");
	if (fd_udp < 1) {
		printf("udp open\n");
		return 1;
	}

	i = 1;
	if (setsockopt(fd_udp, SOL_SOCKET, SO_BROADCAST, &i, sizeof(i)) < 0) {
		printf("setsockopt: %s\n", strerror(errno));
		shutdown(fd_udp, SHUT_RDWR);
		return 1;
	}

	for (m = 0; m < MAVLINK_UDP_MAX; m++)
		mtimer_timeout_clear(&mavlink_udp_list[m].timer);

	while (1) {
		FD_ZERO(&fds);
		FD_SET(fd_uart, &fds);
		FD_SET(fd_udp, &fds);
		tv.tv_sec = 1;
		tv.tv_usec = 0;
		l = select(fd_uart > fd_udp ? (fd_uart + 1) : (fd_udp + 1), &fds, NULL, NULL, &tv);
		if (l < 0)
			break;

		if (FD_ISSET(fd_uart, &fds)) {
			l = read(fd_uart, buffer, 2048);
			if (l < 1)
				break;

			for (i = 0; i < l; i++) {
				if (mavlink_recv_loc == 0) {
					if ((buffer[i] == 0xFE) || (buffer[i] == 0xFD)) {
						mavlink_recv_buffer[mavlink_recv_loc++] = buffer[i];
					} else {
						printf("SKIPPED: %02X\n", (unsigned int)buffer[i]);
					}
				} else {
					if (mavlink_recv_loc == 3) {
						if (mavlink_recv_buffer[0] == 0xFE) {
							mavlink_recv_len = 8 + mavlink_recv_buffer[1];
						} else if (mavlink_recv_buffer[0] == 0xFD) {
							mavlink_recv_len = 12 + mavlink_recv_buffer[1];
							if (mavlink_recv_buffer[2] & 0x01)
								mavlink_recv_len += 13;
						}
					}
					if (mavlink_recv_loc < 500)
						mavlink_recv_buffer[mavlink_recv_loc++] = buffer[i];

					if (mavlink_recv_loc == mavlink_recv_len) {
						mtimer_timeout_set(&uart_timer, 5000);
						if (!uart_ok) {
							uart_ok = 1;
							event(cmd, 0, 1);
						}
						printf("MAVLINK:");
						for (mavlink_recv_loc = 0; mavlink_recv_loc < mavlink_recv_len; mavlink_recv_loc++) {
							printf(" %02X", (unsigned int)mavlink_recv_buffer[mavlink_recv_loc]);
						}
						printf("\n");
						sendto(fd_udp, mavlink_recv_buffer, mavlink_recv_len, 0, result->ai_addr, result->ai_addrlen);
						for (m = 0; m < MAVLINK_UDP_MAX; m++) {
							if (!mtimer_timeout(&mavlink_udp_list[m].timer))
								sendto(fd_udp, mavlink_recv_buffer, mavlink_recv_len, 0, &mavlink_udp_list[m].addr, sizeof(struct sockaddr));
						}
						mavlink_recv_len = 4;
						mavlink_recv_loc = 0;
					}
				}
			}
		}
		if (FD_ISSET(fd_udp, &fds)) {
			t = sizeof(addr);
			l = recvfrom(fd_udp, buffer, 2048, 0, &addr, &t);
			if (l < 1)
				break;

			printf("RECV: %s, %d:", addr_cstr(&addr), l);
			for (i = 0; i < l; i++)
				printf(" %02X", (unsigned int)buffer[i]);

			if (addr_broadcast(&addr))
				printf(" (broadcast)\n");
			else
				printf("\n");

			if (!addr_broadcast(&addr)) {
				write(fd_uart, buffer, l);

				if (addr_isequal(&addr, result->ai_addr)) { // server
					mtimer_timeout_set(&udp_timer, 5000);
					if (!udp_ok) {
						udp_ok = 1;
						event(cmd, 1, 1);
					}
				} else {
					sendto(fd_udp, buffer, l, 0, result->ai_addr, result->ai_addrlen);
					for (m = 0; m < MAVLINK_UDP_MAX; m++) {
						if (addr_isequal(&addr, &mavlink_udp_list[m].addr)) {
							mtimer_timeout_set(&mavlink_udp_list[m].timer, 5000);
							break;
						}
					}
					if (m == MAVLINK_UDP_MAX) {
						for (m = 0; m < MAVLINK_UDP_MAX; m++) {
							if (mtimer_timeout(&mavlink_udp_list[m].timer)) {
								printf("NEW UDP: %s:%u\n", inet_ntoa(((struct sockaddr_in *)&addr)->sin_addr), ntohs(((struct sockaddr_in *)&addr)->sin_port));
								mavlink_udp_list[m].addr = addr;
								mtimer_timeout_set(&mavlink_udp_list[m].timer, 5000);
								break;
							}
						}
					}
				}
				for (m = 0; m < MAVLINK_UDP_MAX; m++) {
					if (!mtimer_timeout(&mavlink_udp_list[m].timer) && !addr_isequal(&addr, &mavlink_udp_list[m].addr))
						sendto(fd_udp, buffer, l, 0, &mavlink_udp_list[m].addr, sizeof(struct sockaddr));
				}
			}
		}
		if (mtimer_timeout(&uart_timer)) {
			if (uart_ok) {
				event(cmd, 0, 0);
				uart_ok = 0;
			}
		}
		if (mtimer_timeout(&udp_timer)) {
			if (udp_ok) {
				event(cmd, 1, 0);
				udp_ok = 0;
			}
		}
		if (mtimer_timeout(&broadcast_timer)) {
			timestamp = 1000000LLU * time(NULL);
			buffer[0] = (timestamp >> 0) & 0xFF;
			buffer[1] = (timestamp >> 8) & 0xFF;
			buffer[2] = (timestamp >> 16) & 0xFF;
			buffer[3] = (timestamp >> 24) & 0xFF;
			buffer[4] = (timestamp >> 32) & 0xFF;
			buffer[5] = (timestamp >> 40) & 0xFF;
			buffer[6] = (timestamp >> 48) & 0xFF;
			buffer[7] = (timestamp >> 56) & 0xFF;
			buffer[8] = (ping_seq >> 0) & 0xFF;
			buffer[9] = (ping_seq >> 8) & 0xFF;
			buffer[10] = (ping_seq >> 16) & 0xFF;
			buffer[11] = (ping_seq >> 24) & 0xFF;
			buffer[12] = 0;
			buffer[13] = 0;
			i = mavlink2(buffer, mav_seq, 0, 90, 4, buffer, 14, 237);
			i = socket_udp_sendto(fd_udp, 0, "192.168.2.25", "14540", buffer, i);
			// i = socket_udp_sendto(fd_udp, 0, "192.168.2.255", "14540", "\xFD\x09\x00\x00\xF2\xFF\xBE\x00\x00\x00\x00\x00\x00\x00\x06\x08\xC0\x04\x03\x4B\x3F", 21);
			printf("SEND PING: %d\n", i);
			ping_seq++;
			mav_seq++;
			mtimer_timeout_set(&broadcast_timer, 1000);
		}
	}

	return 1;
}

static char *addr_cstr(struct sockaddr *addr)
{
	static char buffer[64];

	snprintf(buffer, sizeof(buffer), "%u.%u.%u.%u:%u",
		(unsigned int)((unsigned char)addr->sa_data[2]),
		(unsigned int)((unsigned char)addr->sa_data[3]),
		(unsigned int)((unsigned char)addr->sa_data[4]),
		(unsigned int)((unsigned char)addr->sa_data[5]),
		(unsigned int)((((uint16_t)((unsigned char)addr->sa_data[0])) << 8) | ((unsigned char)addr->sa_data[1]))
	);

	return buffer;
}

static int addr_broadcast(struct sockaddr *addr)
{
	return !memcmp(&addr->sa_data[2], "\xC0\xA8\x02\x14", 4);
}

static int addr_isequal(struct sockaddr *addr1, struct sockaddr *addr2)
{
	return !memcmp(addr1->sa_data, addr2->sa_data, 6);
}

static uint16_t crc16_byte(uint16_t crc, uint8_t data)
{
	uint8_t tmp;

	tmp = data ^ (uint8_t)(crc & 0xFF);
	tmp ^= (tmp << 4);
        return ((crc >> 8) ^ (tmp << 8) ^ (tmp << 3) ^ (tmp >> 4));
}

static int mavlink2(uint8_t *buf, uint8_t seq, uint8_t sys_id, uint8_t comp_id, uint32_t msg_id, uint8_t *payload, int len, uint8_t crc_extra)
{
	uint16_t crc;
	int i;

	memmove(&buf[10], payload, len);

	buf[0] = 0xFD;    // STX
	buf[1] = len;     // LEN
	buf[2] = 0;       // INC FLAGS
	buf[3] = 0;       // CMP FLAGS
	buf[4] = seq;     // SEQ
	buf[5] = sys_id;  // SYS ID
	buf[6] = comp_id; // COMP_ID
	buf[7] = (msg_id >> 0)  & 0xFF;
	buf[8] = (msg_id >> 8)  & 0xFF;
	buf[9] = (msg_id >> 16) & 0xFF;

	crc = 0xFFFF;
	for (i = 0; i < (10 + len); i++)
		crc = crc16_byte(crc, buf[i]);

	crc = crc16_byte(crc, crc_extra);
	buf[10 + len + 0] = (crc >> 0) & 0xFF;
	buf[10 + len + 1] = (crc >> 8) & 0xFF;

	return (10 + len + 2);
}

static void event(char *cmd, int type, int on)
{
	char buffer[256];

	if (cmd == NULL)
		return;

	snprintf(buffer, 256, "%s %d %d", cmd, type, on);
	buffer[255] = 0;
	system(buffer);
}

