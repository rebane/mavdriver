CROSS_COMPILE = /opt/mipsel-openwrt-linux/bin/mipsel-openwrt-linux-

all:
	$(CROSS_COMPILE)gcc -Wall -I. -Iruart ruart/ruart.c ruart/ruart_standard.c ruart/ruart_extended.c -Imtimer mtimer/timer_linux.c mtimer/mtimer.c socket_udp.c main.c -o mavdriver
	$(CROSS_COMPILE)strip mavdriver

