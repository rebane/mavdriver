#ifndef _SOCKET_UDP_H_
#define _SOCKET_UDP_H_

#include <stdio.h>

int socket_udp_bind(char *bind_address, char *bind_port);
ssize_t socket_udp_recvfrom(int socket, int flags, char *recv_address, char *recv_port, void *buf, size_t len);
ssize_t socket_udp_sendto(int socket, int flags, char *send_address, char *send_port, const void *buf, size_t len);
int socket_udp_multicast(int socket, char *local_address, char *multicast_address);
int socket_udp_broadcast(int socket);
int socket_udp_local_port(int socket);
void socket_udp_close(int socket);

#endif

