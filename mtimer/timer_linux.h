#ifndef _TIMER_LINUX_H_
#define _TIMER_LINUX_H_

#include <stdint.h>

void timer_linux_init();
uint32_t timer_linux_get(uint16_t *msec);

#endif

